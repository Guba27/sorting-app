package org.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

/**
 * The Sorting app is Java application that takes up to ten command-line arguments as integer values
 * sorts them in ascending order, then prints them to standard output.
 *
 * @author gubaz
 */
public class App {

    private static final Logger logger = LogManager.getLogger(App.class);

    public static void main( String[] args ){
        //checking if exactly 10 command-line arguments are provided
        int maxArguments = 10;
        if (args.length == 0 ){
            System.out.printf("Provide at leas one argument");
            return;
        } else if (args.length > maxArguments) {
            System.out.printf("Too many arguments, should be max 10");
            return;
        }
        //converting command-Line arguments to integer
        int [] numbers= new int[args.length];
        for (int i = 0; i < args.length; i++){
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e){
                System.out.printf("Error");
                return;
            }
        }
        //sorting the array in ascending order
        Arrays.sort(numbers);

        //printing sorted numbers
        for (int number : numbers) {
            System.out.print(number + " ");
        }
    }
}
