package org.epam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AppTest{
    private String[] inputArgs;
    private String expectedOutput;

    public AppTest(String[] inputArgs, String expectedOutput) {
        this.inputArgs = inputArgs;
        this.expectedOutput = expectedOutput;
    }
    @Parameterized.Parameters(name = "{index}: input={0}, expected={1}")
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, "Provide at least one argument"},
                {new String[]{"5"}, "5"},
                {new String[]{"7", "3", "10", "1", "5", "2", "9", "4", "6", "8"}, "1 2 3 4 5 6 7 8 9 10"},
                {new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"}, "Too many arguments, should be max 10"},
        });
    }
    @Test
    public void testSortingAppWithNoArguments(){
        assertAppOutput(new String[]{}, "Provide at leas one argument");

    }

    @Test
    public void testSortingAppWithOneArgument(){
        assertAppOutput(new String[]{"5"}, "5");
    }
    @Test
    public void testSortingAppWithTenArguments(){
        assertAppOutput(new String[]{"7", "3", "10", "1", "5", "2", "9", "4", "6", "8"}, "1 2 3 4 5 6 7 8 9 10");
    }

    @Test
    public void testSortingAppWithMoreThanTenArguments(){
        assertAppOutput(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"} , "Too many arguments, should be max 10");
    }


    private void assertAppOutput(String[] args, String expectedOutput) {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        App.main(args);
        String actualOutput = outContent.toString().trim();
        assertEquals(expectedOutput, actualOutput);
    }
}
